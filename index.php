<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar OOP</title>
</head>
<body>
    <?php
        echo "<h3>Tugas OOP PHP</h3>";
        require_once('animal.php');
        require_once('frog.php');
        require_once('ape.php');
        $animal = new Animal("Shaun");
        echo "Nama Binatang = " .$animal->name . "<br>";
        echo "Jumlah Kaki = " .$animal->legs . " Kaki" . "<br>";
        echo "Apakah " .$animal->name . " termasuk hewan berdarah dingin = " . $animal->cold_blooded . "<br><br>";

        $katak = new Frog ('Budug');
        echo "Nama Binatang = " .$katak->name . "<br>";
        echo "Jumlah Kaki = " .$katak->legs . " Kaki" . "<br>";
        echo "Apakah " .$katak->name . " termasuk hewan berdarah dingin = " . $katak->cold_blooded . "<br>";
        echo "Jump = " ; echo $katak->jump();
        echo "<br><br>";

        $sungokong = new Ape ('Kera Sakti');
        echo "Nama Binatang = " .$sungokong->name . "<br>";
        echo "Jumlah Kaki = " .$sungokong->legs . " Kaki" . "<br>";
        echo "Apakah " .$sungokong->name . " termasuk hewan berdarah dingin = " . $sungokong->cold_blooded . "<br>";
        echo "Yell = " ; echo $sungokong->yell();

?>
</body>
</html>